CXXFLAGS=-std=c++2a -g -O3 -Wall -pedantic -Ilibs/pstreams -Ilibs/giolib -ldw

all: filter_log

filter_log: filter_log.cpp
	g++ -o filter_log filter_log.cpp ${CXXFLAGS}
