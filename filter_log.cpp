#include <iostream>
#include <regex>
#include <map>
#include <iomanip>
#include <cmath>
#include <optional>
#include <filesystem>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cxxabi.h>

#include <elf.h>

#include <pstream.h>

#include <nlohmann/json.hpp>

#include <giolib/utils.h>
#include <giolib/std_printers.h>
#include <giolib/assert.h>
#include <giolib/exception.h>
#include <giolib/main.h>

static const std::regex BACKTRACE_RE("^backtrace_frame fp=0x([0-9a-fA-F]+), ip=0x([0-9a-fA-F]+), off=0x([0-9a-fA-F]+), file=(.*)$", std::regex::extended);
static const std::regex BACKTRACE_RESOLVED_RE("^backtrace_resolved_frame (.*)$", std::regex::extended);
static const std::regex ADD_MAP_RE("^add_map from=([0-9a-fA-F]+) to=([0-9a-fA-F]+) offset=([0-9a-fA-F]+) path=\"(.*)\" is_wine_code=([01])$", std::regex::extended);
static const std::regex REMOVE_MAP_RE("^remove_map from=([0-9a-fA-F]+) to=([0-9a-fA-F]+)$", std::regex::extended);

static const std::string RED_CMD = "\033[91m";
static const std::string GREEN_CMD = "\033[92m";
static const std::string YELLOW_CMD = "\033[93m";
static const std::string LIGHT_PURPLE_CMD = "\033[94m";
static const std::string PURPLE_CMD = "\033[95m";
static const std::string CYAN_CMD = "\033[96m";
static const std::string LIGHT_GRAY_CMD = "\033[97m";
static const std::string BLACK_CMD = "\033[98m";
static const std::string RESET_CMD = "\033[00m";
static const std::string COLOR8_CMD1 = "\033[38;5;";
static const std::string COLOR8_CMD2 = "m";
static const std::string LINK_CMD1 = "\033]8;;";
static const std::string LINK_CMD2 = "\07";
static const std::string LINK_CMD3 = "\033]8;;\007";

static const std::map<std::string, std::string> class_cmds = {
    {"trace", GREEN_CMD},
    {"debug", PURPLE_CMD},
    {"info", LIGHT_PURPLE_CMD},
    {"fixme", CYAN_CMD},
    {"warn", YELLOW_CMD},
    {"err", RED_CMD},
    {"none", LIGHT_GRAY_CMD},
};

class mmap_state {
public:
    struct map_data {
        uint64_t from, to, offset;
        std::string path;
        bool is_wine_code;

        enum intersection_result {
            BELOW,
            INITIAL_SEGMENT,
            MIDDLE_SEGMENT,
            FINAL_SEGMENT,
            ABOVE,
            SUPERSET,
        };

        map_data(uint64_t from, uint64_t to, uint64_t offset = 0, std::string path = {}, bool is_wine_code = false)
            : from(from), to(to), offset(offset), path(std::move(path)), is_wine_code(is_wine_code) {
            gio_assert(from < to);
            this->is_wine_code = detect_wine_code();
        }

        bool has(uint64_t loc) {
            return this->from <= loc && loc < this->to;
        }

        bool detect_wine_code() {
            if (path == "") {
                return false;
            }
            std::string lower_path = path;
            std::transform(lower_path.begin(), lower_path.end(), lower_path.begin(), tolower);
            if (!lower_path.ends_with(".dll") && !lower_path.ends_with(".exe")) {
                return true;
            }
            if (lower_path.find("/wine/") != std::string::npos) {
                return true;
            }
            return false;
        }

        bool operator<(const map_data &x) const noexcept {
            return this->from < x.from;
        }

        uint64_t offset_addr(uint64_t addr) const noexcept {
            return addr - this->from + this->offset;
        }

        intersection_result operator&&(const map_data &x) const noexcept {
            if (x.to <= this->from) {
                return BELOW;
            }
            if (this->to <= x.from) {
                return ABOVE;
            }
            bool starts_below = x.from <= this->from;
            bool ends_above = x.to >= this->to;
            if (starts_below) {
                if (ends_above) {
                    return SUPERSET;
                } else {
                    return INITIAL_SEGMENT;
                }
            } else {
                if (ends_above) {
                    return FINAL_SEGMENT;
                } else {
                    return MIDDLE_SEGMENT;
                }
            }
        }
    };

private:
    std::map<uint64_t, std::set<map_data>> maps;

public:
    void remove(uint64_t pid, map_data &data) {
        auto &pid_map = this->maps[pid];
        auto it = pid_map.upper_bound(data);
        if (it != pid_map.begin()) {
            --it;
        }
        while (it != pid_map.end()) {
            auto intersect = *it && data;
            if (intersect == map_data::BELOW) {
                break;
            } else if (intersect == map_data::INITIAL_SEGMENT) {
                map_data tmp = *it;
                tmp.offset += data.to - tmp.from;
                tmp.from = data.to;
                gio_assert(tmp.from < tmp.to);
                it = pid_map.erase(it);
                pid_map.insert(it, std::move(tmp));
                break;
            } else if (intersect == map_data::MIDDLE_SEGMENT) {
                map_data tmp1 = *it, tmp2 = *it;
                tmp1.to = data.from;
                gio_assert(tmp1.from < tmp1.to);
                tmp2.offset += data.to - tmp2.from;
                tmp2.from = data.to;
                gio_assert(tmp2.from < tmp2.to);
                it = pid_map.erase(it);
                pid_map.insert(it, std::move(tmp1));
                pid_map.insert(it, std::move(tmp2));
                break;
            } else if (intersect == map_data::FINAL_SEGMENT) {
                map_data tmp = *it;
                tmp.to = data.from;
                gio_assert(tmp.from < tmp.to);
                it = pid_map.erase(it);
                pid_map.insert(it, std::move(tmp));
                continue;
            } else if (intersect == map_data::ABOVE) {
                ++it;
                continue;
            } else {
                gio_assert(intersect == map_data::SUPERSET);
                it = pid_map.erase(it);
                continue;
            }
        }
    }

    void add(uint64_t pid, map_data &data) {
        this->remove(pid, data);
        if (data.path == "") {
            if (const auto close = this->query_to(pid, data.from)) {
                data.path = close->get().path;
            } else if (const auto close = this->query_from(pid, data.to)) {
                data.path = close->get().path;
            } else {
                return;
            }
        }
        this->maps[pid].insert(data);
    }

    std::optional<std::reference_wrapper<const map_data>> query(uint64_t pid, uint64_t addr) const {
        auto it = this->maps.find(pid);
        if (it == this->maps.end()) {
            return {};
        }
        auto &pid_map = it->second;
        map_data data(addr, UINT64_MAX);
        auto it2 = pid_map.upper_bound(data);
        if (it2 != pid_map.begin()) {
            --it2;
            if (it2->from <= addr && addr < it2->to) {
                return {{*it2}};
            }
        }
        return {};
    }

private:
    std::optional<std::reference_wrapper<const map_data>> query_from(uint64_t pid, uint64_t addr) const {
        auto it = this->maps.find(pid);
        if (it == this->maps.end()) {
            return {};
        }
        auto &pid_map = it->second;
        map_data data(addr, UINT64_MAX);
        auto it2 = pid_map.lower_bound(data);
        if (it2 != pid_map.end() && addr == it2->from) {
            return {{*it2}};
        }
        return {};
    }

    std::optional<std::reference_wrapper<const map_data>> query_to(uint64_t pid, uint64_t addr) const {
        auto it = this->maps.find(pid);
        if (it == this->maps.end()) {
            return {};
        }
        auto &pid_map = it->second;
        map_data data(addr, UINT64_MAX);
        auto it2 = pid_map.upper_bound(data);
        if (it2 != pid_map.begin()) {
            --it2;
            if (addr == it2->to) {
                return {{*it2}};
            }
        }
        return {};
    }
};

std::string demangle(const std::string &s) {
    int status = 0;
    char *tmp = abi::__cxa_demangle(s.c_str(), 0, 0, &status);
    if (status == 0) {
        std::string ret(tmp);
        free(tmp);
        return ret;
    } else {
        return s;
    }
}

struct log_line {
    double timestamp;
    uint32_t pid;
    uint32_t tid;
    std::string_view klass;
    std::string_view channel;
    std::string_view arch;
    uint64_t retaddr;
    std::string_view file;
    uint64_t line_num;
    std::string_view func;
    std::string_view msg;
    std::string_view header;
    std::string_view line;
    bool valid;

    static log_line from_line(const std::string_view &line) {
        log_line data;
        data.line = line;

        // Discard leading spaces
        size_t pos = line.find_first_not_of(" ");

        const size_t space_pos = line.find(' ', pos);
        if (space_pos == std::string_view::npos)
            return from_invalid_line(line);

        data.header = line.substr(pos, space_pos - pos);

        if (!read_field(line, pos, space_pos, data.timestamp))
            return from_invalid_line(line);
        if (!read_field(line, pos, space_pos, data.pid, 16))
            return from_invalid_line(line);
        if (!read_field(line, pos, space_pos, data.tid, 16))
            return from_invalid_line(line);
        if (!read_field(line, pos, space_pos, data.klass))
            return from_invalid_line(line);
        if (!read_field(line, pos, space_pos, data.channel))
            return from_invalid_line(line);
        if (!read_field(line, pos, space_pos, data.arch))
            return from_invalid_line(line);
        if (!read_field(line, pos, space_pos, data.retaddr, 16))
            return from_invalid_line(line);
        if (!read_field(line, pos, space_pos, data.file))
            return from_invalid_line(line);
        if (!read_field(line, pos, space_pos, data.line_num, 10))
            return from_invalid_line(line);
        if (!read_field(line, pos, space_pos, data.func, 0, ' '))
            return from_invalid_line(line);

        if (pos != space_pos + 1)
            return from_invalid_line(line);

        data.msg = line.substr(pos);
        data.valid = true;

        return data;
    }

private:
    static bool decode_field(const std::string_view &field_str, double &field, int base) {
        (void)base;
        size_t pos;
        field = std::stod(std::string(field_str), &pos);
        return pos == field_str.size();
    }

    static bool decode_field(const std::string_view &field_str, unsigned &field, int base) {
        size_t pos;
        assert(base != 0);
        field = std::stoul(std::string(field_str), &pos, base);
        return pos == field_str.size();
    }

    static bool decode_field(const std::string_view &field_str, unsigned long &field, int base) {
        size_t pos;
        assert(base != 0);
        field = std::stoul(std::string(field_str), &pos, base);
        return pos == field_str.size();
    }

    static bool decode_field(const std::string_view &field_str, unsigned long long &field, int base) {
        size_t pos;
        assert(base != 0);
        field = std::stoull(std::string(field_str), &pos, base);
        return pos == field_str.size();
    }

    static bool decode_field(const std::string_view &field_str, std::string_view &field, int base) {
        (void)base;
        field = field_str;
        return true;
    }

    template<typename T>
    static bool read_field(const std::string_view &line, size_t &pos, const size_t space_pos, T &field, int base = 0, const char term = ':') {
        size_t new_pos = line.find(term, pos);
        if (pos == std::string_view::npos || pos > space_pos)
            return false;
        try {
            if (!decode_field(line.substr(pos, new_pos - pos), field, base))
                return false;
        } catch (...) {
            return false;
        }
        pos = new_pos + 1;
        return true;
    }

    static log_line from_invalid_line(std::string_view line) {
        log_line data{};
        data.msg = line;
        data.line = line;
        return data;
    }
};

std::tuple<std::string, std::string_view, std::string_view> format_header(const std::string_view &line, uint64_t &pid, mmap_state &state) {
    static double last_timestamp = 0.0;
    log_line data = log_line::from_line(line);

    if (data.valid) {
        pid = data.pid;

        const std::string *class_cmd = &RESET_CMD;
        auto class_it = class_cmds.find(std::string(data.klass));
        if (class_it != std::end(class_cmds)) {
            class_cmd = &class_it->second;
        }

        // const std::string *retaddr_cmd = &RESET_CMD;
        // if (data.retaddr[0] == '!') {
        //     retaddr_cmd = &YELLOW_CMD;
        //     prev_pos++;
        // }

        double elapsed = std::max(0.000001, data.timestamp - last_timestamp);
        auto timestamp_color = std::clamp<int>((std::log(elapsed) + 13.0) * (12.0 / 10.0), 0, 12);
        last_timestamp = data.timestamp;

        std::ostringstream oss;
        oss << COLOR8_CMD1 << std::dec << (255 - timestamp_color) << COLOR8_CMD2
            << std::fixed << std::setprecision(6) << data.timestamp << RESET_CMD << ':' //<< std::log(elapsed) << ':'
            << std::hex << std::setw(4) << std::setfill('0') << data.pid << ':'
            << std::hex << std::setw(4) << std::setfill('0') << data.tid << ':'
            << *class_cmd << data.klass << RESET_CMD << ':'
            << data.channel << ':' << data.arch << ':';
        if (data.channel.size() == 0 || data.channel[0] != '@') {
            const auto &map_item = state.query(data.pid, data.retaddr);
            //oss << *retaddr_cmd;
            if (map_item) {
                if (!map_item->get().is_wine_code) {
                    oss << YELLOW_CMD;
                }
                //oss << LINK_CMD1 << "Called from " << map_item->get().path << LINK_CMD2;
            }
            oss << std::hex << data.retaddr;
            if (map_item) {
                //oss << LINK_CMD3;
            }
            oss << RESET_CMD << ':' << data.file << ':' << std::dec << data.line_num << ':' << data.func;
        }
        oss << ' ';
        return std::make_tuple(oss.str(), data.header,data.msg);
    } else {
        return std::make_tuple(std::string{}, std::string_view{}, line);
    }
}

struct symbol_key_t {
    const std::string &obj_filename;
    unsigned long long off;

    bool operator<(const symbol_key_t &x) const {
        if (this->off < x.off) return true;
        if (x.off < this->off) return false;
        return this->obj_filename < x.obj_filename;
    }
};

struct symbol_elem_t {
    std::string function;
    std::string filename;
    uint64_t line_num;
};

void to_json(nlohmann::json &json, const symbol_elem_t &elem) {
    json = nlohmann::json{
        {"function", elem.function},
        {"filename", elem.filename},
        {"line_num", elem.line_num}
    };
}

void from_json(const nlohmann::json &json, symbol_elem_t &elem) {
    elem.function = json.at("function");
    elem.filename = json.at("filename");
    elem.line_num = json.at("line_num");
}

std::map<symbol_key_t, std::pair<std::vector<symbol_elem_t>, unsigned long long>> symbol_cache;

std::optional<uint64_t> fix_pe_offset(const std::string &filename, uint64_t offset) {
    try {
        std::ifstream fin(filename, std::ios::binary);
        fin.exceptions(std::ios::badbit | std::ios::failbit | std::ios::eofbit);
        if (fin.get() != 'M' || fin.get() != 'Z') {
            return {};
        }
        fin.ignore(58);
        char buf[sizeof(uint32_t)];
        fin.read(buf, sizeof(buf));
        auto pe_off = *reinterpret_cast<uint32_t*>(buf);
        fin.ignore(pe_off - 64);
        if (fin.get() != 'P' || fin.get() != 'E' || fin.get() != '\0' || fin.get() != '\0') {
            return {};
        }
        fin.ignore(20);
        if (fin.get() != '\x0b')
            return {};
        const auto format = fin.get();
        if (format == '\x02') {
            fin.ignore(22);
            char buf2[sizeof(uint64_t)];
            fin.read(buf2, sizeof(buf2));
            auto image_base = *reinterpret_cast<uint64_t*>(buf2);
            return {image_base + offset};
        } else if (format == '\x01') {
            fin.ignore(26);
            fin.read(buf, sizeof(buf));
            auto image_base = *reinterpret_cast<uint32_t*>(buf);
            return {image_base + offset};
        } else {
            return {};
        }
    } catch (...) {
        return {};
    }
}

std::string_view cut_path(std::string_view path, const std::string &cut) {
    auto pos = path.find(cut);
    if (pos != std::string::npos) {
        path.remove_prefix(pos + cut.size());
    }
    return path;
}

std::optional<uint64_t> fix_elf_offset(const std::string &filename, uint64_t phys_off) {
    std::ifstream fin(filename, std::ios::binary);
    if (fin.get() != ELFMAG0 || fin.get() != ELFMAG1 || fin.get() != ELFMAG2 || fin.get() != ELFMAG3) {
        return {};
    }
    char elf_class = fin.get();
    if (elf_class == ELFCLASS32) {
        Elf32_Ehdr header;
        fin.seekg(0);
        fin.read(reinterpret_cast<char*>(&header), sizeof(header));
        for (size_t i = 0; i < header.e_phnum; i++) {
            fin.seekg(header.e_phoff + i * header.e_phentsize);
            Elf32_Phdr section;
            fin.read(reinterpret_cast<char*>(&section), sizeof(section));
            if (section.p_type == PT_LOAD && section.p_offset <= phys_off && phys_off <= section.p_offset + section.p_filesz) {
                return {phys_off - section.p_offset + section.p_vaddr};
            }
        }
    } else if (elf_class == ELFCLASS64) {
        Elf64_Ehdr header;
        fin.seekg(0);
        fin.read(reinterpret_cast<char*>(&header), sizeof(header));
        for (size_t i = 0; i < header.e_phnum; i++) {
            fin.seekg(header.e_phoff + i * header.e_phentsize);
            Elf64_Phdr section;
            fin.read(reinterpret_cast<char*>(&section), sizeof(section));
            if (section.p_type == PT_LOAD && section.p_offset <= phys_off && phys_off <= section.p_offset + section.p_filesz) {
                return {phys_off - section.p_offset + section.p_vaddr};
            }
        }
    }
    return {};
}

static bool is_regular_file(const std::string &filename) {
    struct stat s;
    return stat(filename.c_str(), &s) == 0 && S_ISREG(s.st_mode);
}

template<typename T>
bool decode_optional(const std::optional<T> &opt, T &value) {
    if (opt) {
        value = *opt;
    }
    return opt.has_value();
}

static std::vector<symbol_elem_t> parse_addr2line_output(const std::vector<std::string> &lines) {
    std::vector<symbol_elem_t> ret;
    for (size_t i = 0; i < lines.size() / 2; i++) {
        ret.push_back({std::move(lines.at(2*i)), std::move(lines.at(2*i+1)), 0});
        auto &elem = ret.back();
        auto colon_pos = elem.filename.find_last_of(":");
        if (colon_pos != std::string::npos) {
            try {
                elem.line_num = std::stoull(elem.filename.substr(colon_pos+1));
            } catch (...) {
                // Swallow invalid line number
            }
            elem.filename.resize(colon_pos);
        }
        if (elem.function == "??") {
            return {};
        }
    }
    return ret;
}

static std::vector<symbol_elem_t> resolve_symbol_pdb_addr2line(const std::string &obj_filename, uint64_t off) {
    std::string pdb_filename = obj_filename;
    if (pdb_filename.ends_with(".exe") || pdb_filename.ends_with(".EXE") || pdb_filename.ends_with(".dll") || pdb_filename.ends_with(".DLL")) {
        pdb_filename = pdb_filename.substr(0, pdb_filename.size() - 4);
    }
    pdb_filename += ".pdb";
    if (!is_regular_file(pdb_filename)) {
        return {};
    }
    std::vector<std::string> lines;
    {
        std::vector<std::string> argv = {"pdb-addr2line", "-fiC", "-e", pdb_filename, gio_make_string(std::hex << (off - 1))};
        redi::ipstream proc(argv, redi::pstreams::pstdout);
        for (std::string line; std::getline(proc, line);) {
            lines.push_back(line);
        }
    }
    return parse_addr2line_output(lines);
}

static std::vector<symbol_elem_t> resolve_symbol_addr2line(const std::string &obj_filename, uint64_t off) {
    std::vector<std::string> lines;
    {
        std::vector<std::string> argv = {"addr2line", "-fi", "-e", obj_filename, gio_make_string(std::hex << (off - 1))};
        redi::ipstream proc(argv, redi::pstreams::pstdout);
        for (std::string line; std::getline(proc, line);) {
            lines.push_back(line);
        }
    }
    return parse_addr2line_output(lines);
}

static std::vector<symbol_elem_t> resolve_symbol_nm(const std::string &obj_filename, uint64_t off) {
    std::vector<std::string> argv = {"nm", obj_filename};
    redi::ipstream proc(argv, redi::pstreams::pstdout);
    for (std::string line; std::getline(proc, line);) {
        if (line == "" || line.at(0) == ' ') {
            continue;
        }
        size_t after;
        unsigned long long line_addr;
        try {
            line_addr = std::stoull(line, &after, 16);
        } catch (...) {
            continue;
        }
        if (line_addr != off) {
            continue;
        }
        symbol_elem_t elem;
        elem.function = line.substr(after + 3);
        return {elem};
    }
    return {};
}

const std::pair<std::vector<symbol_elem_t>, unsigned long long> &resolve_symbol(const std::string &obj_filename, uint64_t off) {
    using namespace gio::std_printers;
    const symbol_key_t key{obj_filename, off};
    auto it = symbol_cache.find(key);
    uint64_t logical_off = 0;
    if (it == symbol_cache.end()) {
        std::vector<symbol_elem_t> ret;
        if (decode_optional(fix_pe_offset(obj_filename, off), logical_off)) {
            ret = resolve_symbol_pdb_addr2line(obj_filename, off);
            if (ret.empty()) {
                ret = resolve_symbol_addr2line(obj_filename, logical_off);
            }
            if (ret.empty()) {
                ret = resolve_symbol_nm(obj_filename, logical_off);
            }
        } else if (decode_optional(fix_elf_offset(obj_filename, off), logical_off)) {
            ret = resolve_symbol_addr2line(obj_filename, logical_off);
            if (ret.empty()) {
                ret = resolve_symbol_nm(obj_filename, logical_off);
            }
        }
        symbol_cache[key] = std::make_pair(ret, logical_off);
        it = symbol_cache.find(key);
    }
    return it->second;
}

const std::vector<std::pair<std::string, std::string>> SUBSTS{
    {"Test marked todo", CYAN_CMD},
    {"Test succeeded inside todo block", LIGHT_PURPLE_CMD},
    {"Test failed", RED_CMD},
    {"Test succeeded", GREEN_CMD},
};

static const std::regex COMPAT_TOOLS_RE("/compatibilitytools\\.d/");
static const std::regex SYSTEM32_RE("/pfx/drive_c/windows/system32/");
static const std::regex SYSWOW64_RE("/pfx/drive_c/windows/syswow64/");

std::string find_local_file(const std::string &filename) {
    if (is_regular_file(filename)) {
        return filename;
    }

    const std::string_view run_host_prefix = "/run/host/";
    std::string suffix;
    if (filename.starts_with(run_host_prefix)) {
        suffix = filename.substr(run_host_prefix.size() - 1);
        if (is_regular_file(suffix)) {
            return suffix;
        }
    } else {
        suffix = filename;
    }

    static const std::string compat_tools_dir = "/home/giovanni/.steam/steam/compatibilitytools.d/";
    std::match_results<std::string::const_iterator> match;
    if (std::regex_search(std::cbegin(filename), std::cend(filename), match, COMPAT_TOOLS_RE)) {
        const std::string mangled = compat_tools_dir + match.suffix().str();
        if (is_regular_file(mangled)) {
            return mangled;
        }
    }

    static const std::string system32_dir = "/home/giovanni/.steam/steam/compatibilitytools.d/gio-local/dist/lib64/wine/dxvk/";
    if (std::regex_search(std::cbegin(filename), std::cend(filename), match, SYSTEM32_RE)) {
        const std::string mangled = system32_dir + match.suffix().str();
        if (is_regular_file(mangled)) {
            return mangled;
        }
    }

    static const std::string syswow64_dir = "/home/giovanni/.steam/steam/compatibilitytools.d/gio-local/dist/lib/wine/dxvk/";
    if (std::regex_search(std::cbegin(filename), std::cend(filename), match, SYSWOW64_RE)) {
        const std::string mangled = syswow64_dir + match.suffix().str();
        if (is_regular_file(mangled)) {
            return mangled;
        }
    }

    static const std::string vkd3d_proton_system32_dir = "/home/giovanni/.steam/steam/compatibilitytools.d/gio-local/dist/lib64/wine/vkd3d-proton/";
    if (std::regex_search(std::cbegin(filename), std::cend(filename), match, SYSTEM32_RE)) {
        const std::string mangled = vkd3d_proton_system32_dir + match.suffix().str();
        if (is_regular_file(mangled)) {
            return mangled;
        }
    }

    static const std::string vkd3d_proton_syswow64_dir = "/home/giovanni/.steam/steam/compatibilitytools.d/gio-local/dist/lib/wine/vkd3d-proton/";
    if (std::regex_search(std::cbegin(filename), std::cend(filename), match, SYSWOW64_RE)) {
        const std::string mangled = vkd3d_proton_syswow64_dir + match.suffix().str();
        if (is_regular_file(mangled)) {
            return mangled;
        }
    }

    static const std::string vkd3d_system32_dir = "/home/giovanni/.steam/steam/compatibilitytools.d/gio-local/dist/lib64/vkd3d/";
    if (std::regex_search(std::cbegin(filename), std::cend(filename), match, SYSTEM32_RE)) {
        const std::string mangled = vkd3d_system32_dir + match.suffix().str();
        if (is_regular_file(mangled)) {
            return mangled;
        }
    }

    static const std::string vkd3d_syswow64_dir = "/home/giovanni/.steam/steam/compatibilitytools.d/gio-local/dist/lib/vkd3d/";
    if (std::regex_search(std::cbegin(filename), std::cend(filename), match, SYSWOW64_RE)) {
        const std::string mangled = vkd3d_syswow64_dir + match.suffix().str();
        if (is_regular_file(mangled)) {
            return mangled;
        }
    }

    static const std::string local_copy_dir = "local_copy";
    std::string local_copy = local_copy_dir + suffix;
    if (is_regular_file(local_copy)) {
        return local_copy;
    }

    return filename;
}

struct line_data {
    std::string line;
    uint64_t log_line_num;
    uint64_t pid;
    std::string_view orig_header;
    std::string header;
    std::string message;
    uint64_t ip;
    uint64_t fp;
    uint64_t offset;
    std::string obj_filename;
    uint64_t logical_offset;
    std::vector<symbol_elem_t> stack;
    bool has_mmap_data;
    bool has_backtrace_data;
    bool has_resolved_backtrace_data;

    line_data(uint64_t log_line_num) : log_line_num(log_line_num), has_mmap_data(false), has_backtrace_data(false), has_resolved_backtrace_data(false) {
    }

    bool resolve() {
        if (this->has_backtrace_data && !this->has_resolved_backtrace_data) {
            std::tie(this->stack, this->logical_offset) = resolve_symbol(find_local_file(this->obj_filename), this->offset);
            if (!this->stack.empty()) {
                this->has_resolved_backtrace_data = true;
                return true;
            }
        }
        return false;
    }

    void print(std::ostream &os) const {
        if (this->has_resolved_backtrace_data) {
            nlohmann::json data;
            data["ip"] = this->ip;
            data["fp"] = this->fp;
            data["offset"] = this->offset;
            data["obj_filename"] = this->obj_filename;
            data["logical_offset"] = this->logical_offset;
            data["stack"] = this->stack;
            os << this->orig_header << " backtrace_resolved_frame " << data << "\n";
        } else {
            os << this->orig_header << (this->orig_header.empty() ? "" : " ") << this->message << "\n";
        }
    }

    void pretty_print(std::ostream &os) const {
        os << std::dec << this->log_line_num << " ";
        os << this->header;
        if (this->has_resolved_backtrace_data) {
            bool first = true;
            for (const auto &elem : this->stack) {
                if (first) {
                    os << "frame " << YELLOW_CMD << demangle(elem.function) << RESET_CMD << " in " << cut_path(elem.filename, "/wine/") << ":" << elem.line_num << " (" << std::hex << this->offset << " -> " << this->logical_offset << " in object " << cut_path(cut_path(this->obj_filename, "/steamapps/"), "/compatibilitytools.d/") << ", ip " << this->ip << " fp " << this->fp << std::dec << ")\n";
                } else {
                    os << std::dec << this->log_line_num << " ";
                    os << this->header;
                    os << "      " << PURPLE_CMD << demangle(elem.function) << RESET_CMD << " in " << elem.filename << ":" << elem.line_num << "\n";
                }
                first = false;
            }
        } else if (this->has_backtrace_data) {
            os << "frame " << YELLOW_CMD << "??" << RESET_CMD << " (" << std::hex << this->offset << " in object " << cut_path(cut_path(this->obj_filename, "/steamapps/"), "/compatibilitytools.d/") << ", ip=" << this->ip << " fp " << this->fp << std::dec << ")\n";
        } else {
            bool done = false;
            for (const auto &subst : SUBSTS) {
                auto pos = this->message.find(subst.first);
                if (pos != std::string::npos) {
                    const auto sv = std::string_view(this->message);
                    os << sv.substr(0, pos) << subst.second << sv.substr(pos, subst.first.size()) << RESET_CMD << sv.substr(pos + subst.first.size()) << "\n";
                    done = true;
                    break;
                }
            }
            if (!done) {
                os << this->message << "\n";
            }
        }
    }
};

class log_processor {
    mmap_state state;
    uint64_t line_num;
    line_data current;
    std::optional<uint64_t> monitor_location;

public:
    log_processor() : line_num(0), current(0) {
    }

    void set_monitor_location(uint64_t location) {
        this->monitor_location = location;
    }

    void read_line(std::string &&new_line) {
        this->line_num++;
        this->current = line_data(this->line_num);
        this->current.line = std::move(new_line);
        std::tie(this->current.header, this->current.orig_header, this->current.message) = format_header(this->current.line, this->current.pid, this->state);
    }

    bool process_mmap_line() {
        std::match_results<decltype(this->current.message)::const_iterator> match;
        if (std::regex_match(std::cbegin(this->current.message), std::cend(this->current.message), match, ADD_MAP_RE)) {
            mmap_state::map_data data(std::stoull(match[1], nullptr, 16),
                                      std::stoull(match[2], nullptr, 16),
                                      std::stoull(match[3], nullptr, 16),
                                      match[4],
                                      match[5] == "1");
            this->state.add(this->current.pid, data);
            this->current.has_mmap_data = true;
            if (this->monitor_location && data.has(*this->monitor_location)) {
                return false;
            }
            return true;
        } else if (std::regex_match(std::cbegin(this->current.message), std::cend(this->current.message), match, REMOVE_MAP_RE)) {
            mmap_state::map_data data(std::stoull(match[1], nullptr, 16),
                                      std::stoull(match[2], nullptr, 16));
            this->state.remove(this->current.pid, data);
            this->current.has_mmap_data = true;
            if (this->monitor_location && data.has(*this->monitor_location)) {
                return false;
            }
            return true;
        }
        return false;
    }

    bool process_backtrace_line() {
        std::match_results<decltype(this->current.message)::const_iterator> match;
        if (std::regex_match(std::cbegin(this->current.message), std::cend(this->current.message), match, BACKTRACE_RE)) {
            this->current.fp = std::stoull(match[1], nullptr, 16);
            this->current.ip = std::stoull(match[2], nullptr, 16);
            this->current.offset = std::stoull(match[3], nullptr, 16);
            this->current.obj_filename = match[4];
            if (this->current.offset == 0 || true) {
                const auto &map_item = this->state.query(this->current.pid, this->current.ip);
                if (map_item) {
                    this->current.offset = map_item->get().offset_addr(this->current.ip);
                    this->current.obj_filename = map_item->get().path;
                }
            }
            this->current.has_backtrace_data = true;
            return true;
        } else if (std::regex_match(std::cbegin(this->current.message), std::cend(this->current.message), match, BACKTRACE_RESOLVED_RE)) {
            std::string json_str(match[1].first, match[1].second);
            auto json = nlohmann::json::parse(json_str);
            this->current.fp = json.at("fp");
            this->current.ip = json.at("ip");
            this->current.offset = json.at("offset");
            this->current.obj_filename = std::move(json.at("obj_filename"));
            this->current.logical_offset = json.at("logical_offset");
            this->current.stack.clear();
            for (auto &elem : json.at("stack")) {
                this->current.stack.push_back(std::move(elem));
            }
            this->current.has_backtrace_data = true;
            this->current.has_resolved_backtrace_data = true;
            return true;
        }
        return false;
    }

    bool resolve_backtrace_line() {
        return this->current.resolve();
    }

    void print_line() const {
        this->current.print(std::cout);
    }

    void pretty_print_line() const {
        this->current.pretty_print(std::cout);
    }

    const mmap_state &get_state() const {
        return this->state;
    }

    const line_data &get_current_line_data() const {
        return this->current;
    }

    uint64_t get_line_num() const {
        return this->line_num;
    }
};

int pretty_main(fire::optional<std::string> location = fire::arg({"-L", "--location", "memory location to monitor"})) {
    log_processor processor;
    if (location) {
        processor.set_monitor_location(std::stoull(location.value(), nullptr, 16));
    }
    for (std::string line; std::getline(std::cin, line);) {
        processor.read_line(std::move(line));
        if (processor.process_mmap_line()) {
            continue;
        }
        if (processor.process_backtrace_line()) {
            processor.resolve_backtrace_line();
        }
        processor.pretty_print_line();
    }
    return 0;
}

GIO_FIRE_STD(pretty);

GIO_MAIN(filter) {
    log_processor processor;
    for (std::string line; std::getline(std::cin, line);) {
        processor.read_line(std::move(line));
        if (processor.process_mmap_line()) {
            continue;
        }
        if (processor.process_backtrace_line()) {
            processor.resolve_backtrace_line();
        }
        processor.print_line();
    }
    return 0;
}

GIO_MAIN(query) {
    std::vector<std::string> args;
    for (int i = 1; i < argc; i++) {
        args.emplace_back(argv[i]);
    }
    uint64_t query_line = std::stoull(args.at(0), nullptr, 0);
    uint64_t query_pid = std::stoull(args.at(1), nullptr, 0);
    uint64_t query_address = std::stoull(args.at(2), nullptr, 0);
    log_processor processor;
    for (std::string line; std::getline(std::cin, line);) {
        processor.read_line(std::move(line));
        if (processor.process_mmap_line()) {
            continue;
        }
        if (processor.process_backtrace_line()) {
            processor.resolve_backtrace_line();
        }
        if (processor.get_line_num() == query_line) {
            line_data line(query_line);
            line.fp = 0;
            line.ip = query_address;
            line.pid = query_pid;
            const auto &map_data = processor.get_state().query(line.pid, line.ip);
            if (map_data) {
                line.offset = map_data->get().offset_addr(line.ip);
                line.obj_filename = map_data->get().path;
            }
            line.has_backtrace_data = true;
            line.resolve();
            line.pretty_print(std::cout);
            break;
        }
    }
    return 0;
}

GIO_MAIN(tkill) {
    std::string cmdline(argv[1]);
    for (const auto &entry : std::filesystem::directory_iterator{"/proc"}) {
        std::filesystem::path entry_path(entry);
        std::ifstream entry_cmdline_stream(entry_path / "cmdline");
        std::string entry_cmdline;
        entry_cmdline_stream >> entry_cmdline;
        const auto null_pos = entry_cmdline.find('\0');
        if (null_pos != std::string::npos) {
            entry_cmdline.resize(null_pos);
        }
        if (entry_cmdline == cmdline) {
            pid_t pid = std::stoull(entry_path.filename(), nullptr, 10);
            std::cout << "Sending USR2 to process " << pid << "\n";
            for (const auto &thread_entry : std::filesystem::directory_iterator{entry_path / "task"}) {
                std::filesystem::path thread_path(thread_entry);
                pid_t tid = std::stoull(thread_path.filename(), nullptr, 10);
                std::cout << "Sending USR2 to thread " << tid << "\n";
                tgkill(pid, tid, SIGUSR2);
            }
            return 0;
        }
    }
    return 1;
}

int main(int argc, char *argv[]) {
    gio::install_exception_handler();
    std::ios::sync_with_stdio(false);
    return gio::main(argc, argv, false, true, "pretty");
}
