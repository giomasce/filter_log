#To enable these settings, name this file "user_settings.py".

#Settings here will take effect for all games run in this Proton version.

user_settings = {
    #By default, logs are saved to $HOME/steam-<STEAM_GAME_ID>.log, overwriting any previous log with that name.
    #Log directory can be overridden with $PROTON_LOG_DIR.

    #Wine debug logging
    "WINEDEBUG": "+microsecs,+pid,+tid,+debugstr,+loaddll,+mscoree,+gio,+msgbox,+stepping,+signal,+startcmd,+backtrace",

    #DXVK debug logging
    "DXVK_LOG_LEVEL": "info",

    #vkd3d debug logging
    "VKD3D_DEBUG": "warn",

    #wine-mono debug logging (Wine's .NET replacement)
    "WINE_MONO_TRACE": "E:System.NotImplementedException",
    "MONO_LOG_LEVEL": "info",

    #general purpose media logging
    #"GST_DEBUG": "4",
    #or, verbose converter logging (may impact playback performance):
    #"GST_DEBUG": "4,protonaudioconverter:6,protonaudioconverterbin:6,protonvideoconverter:6",

    #Enable DXVK's HUD
    "DXVK_HUD": "full",

    #Use OpenGL-based wined3d for d3d11, d3d10, and d3d9 instead of Vulkan-based DXVK
#    "PROTON_USE_WINED3D": "1",

    #Disable d3d11 entirely
#    "PROTON_NO_D3D11": "1",

    #Disable eventfd-based in-process synchronization primitives
#    "PROTON_NO_ESYNC": "1",

    #Disable futex-based in-process synchronization primitives
#    "PROTON_NO_FSYNC": "1",

    "PROTON_DUMP_DEBUG_COMMANDS": "1",
    "PROTON_LOG": "1",
    "PROTON_LOG_DIR": "/tmp/steam",
    #"LD_PRELOAD": "/home/giovanni/progetti/windows/layer/layer32.so:/home/giovanni/progetti/windows/layer/layer64.so",
    #"WINEDLLOVERRIDES": "dxgi=n",
    "DXVK_SHADER_DUMP_PATH": "/tmp/shaders-dxvk",
    "VKD3D_SHADER_DUMP_PATH": "/tmp/shaders-vkd3d",
    #'__NV_PRIME_RENDER_OFFLOAD': '1',
    #'__GLX_VENDOR_LIBRARY_NAME': 'nvidia',
    #'DXVK_FILTER_DEVICE_NAME': "Quadro",
    #'VKD3D_VULKAN_DEVICE': '0',

    #'VK_LOADER_DEBUG': 'error,warn,info',
    #'VK_INSTANCE_LAYERS': 'VK_LAYER_KHRONOS_validation',
    #'VK_ICD_FILENAMES': '/usr/share/vulkan/icd.d/intel_icd.x86_64.json',

    #'STEAM_OVERLAY_LOGGING': '1',

    #"PROTON_FORCE_LARGE_ADDRESS_AWARE": '0',
}
